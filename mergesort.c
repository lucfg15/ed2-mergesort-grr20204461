//Author: Lucas Frison Goncalves
//GRR20204461

#include <stdio.h>
#include <stdlib.h>

void insertion_sort(int * v, int n);

void merge(int array[], int left, int m, int right) {
    int i, j, k;
    int rightStart = m + 1;
 
    //array pré ordenado
    if (array[rightStart] >= array[m]) return;

    while(left <= m && rightStart <= right) {
        if (array[left] <= array[rightStart]) {
            left++;
        } else {
            int valor = array[rightStart];
            int indice = rightStart;
  
            while(indice != left) {
                array[indice] = array[indice - 1];
                indice--;
            }
            array[left] = valor;
  
            left++;
            m++;
            rightStart++;
        }
    }

}
 
void mergeSort(int arr[], int left, int right) {
    const int tooSmall = 20;
    //coleção pequena usando insertion
    if ((right - left) <= tooSmall) {
        insertion_sort(arr, right);
        return;
    }
    if (left < right) {
        int m = left + (right - left) / 2;
        mergeSort(arr, left, m);
        mergeSort(arr, m + 1, right);
        merge(arr, left, m, right);
    }
}

void insertion_sort(int * v, int n){
    int i, j, aux, x;
	
	for(i=+1; i<n; i++){
		x = v[i];
		j = i;
		while(x < v[j-1] && j > 0){
			v[j] = v[j-1];
			j--;
		}
		v[j] = x;
	}
}

int main(int argc, char const *argv[])
{
    int array[] = {12, 5, 45, 7, 1, 0, 678, 8464, 36, 45, 22, 197645, 11, 154, 13487, 13, 111, 155, 154, 3, 65, 78};
    mergeSort(array, 0, 22); 
    for (int i = 0; i < 22; i++)
        printf("%d\t", array[i]);
    return 0;
}
